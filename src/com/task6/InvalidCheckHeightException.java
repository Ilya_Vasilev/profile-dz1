package com.task6;

public class InvalidCheckHeightException extends Exception{

    public InvalidCheckHeightException(){
        super("Рост указан неверно");
    }
}
