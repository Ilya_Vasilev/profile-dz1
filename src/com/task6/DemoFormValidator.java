package com.task6;

public class DemoFormValidator {
    public static void main(String[] args) {

        try {
            FormValidator.checkName("Ilya");
        } catch (InvalidNameException e) {
            System.out.println(e.getMessage());
        }

        try {
            FormValidator.checkBirthdate("31.12.2022");
        } catch (InvalidCheckBirthdateException e) {
            System.out.println(e.getMessage());
        }

        try {
            FormValidator.checkGender("MALE");
        } catch (InvalidCheckGenderException e) {
            System.out.println(e.getMessage());
        }

        try {
            FormValidator.checkHeight("-1");
        } catch (InvalidCheckHeightException e) {
            System.out.println(e.getMessage());
        }
    }
}
