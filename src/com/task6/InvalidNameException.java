package com.task6;

public class InvalidNameException extends Exception{

    public InvalidNameException(){
        super("Имя указано неверно");
    }

}
