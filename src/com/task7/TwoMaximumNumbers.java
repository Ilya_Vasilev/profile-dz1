package com.task7;

import java.util.Arrays;
import java.util.Scanner;

public class TwoMaximumNumbers {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int [] arr = new int[n];

        for (int i = 0; i<arr.length; i++){
            arr[i] = in.nextInt();
        }
        Arrays.sort(arr);
        System.out.println(arr[arr.length-1] + " " + arr[arr.length-2]);

    }
}
