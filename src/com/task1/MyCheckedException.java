package com.task1;

import java.io.FileNotFoundException;

public class MyCheckedException extends FileNotFoundException {
    public MyCheckedException(){
        super("Сработало исключение MyCheckedException");
    }
    public MyCheckedException(String message){
        super(message);
    }

}
