package com.task1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class DemoMyCheckedException {
    public static void main(String[] args) throws MyCheckedException {
        try {
            Scanner scanner = new Scanner(new File("test.txt"));
        } catch (FileNotFoundException e) {
            throw new MyCheckedException();
        }
        finally {
            System.out.println("Выход из программы");
        }
    }
}